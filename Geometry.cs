using System;
using System.Collections.Generic;
using GeoAPI.Geometries;

namespace DistanceMap
{
    static class Geometry
    {
        public static double ImageCoordinateToMapCoordinate(int imgPosition, int imgRes, double coordinateStart, double coordinateRange)
        {
            var fraction = (double)imgPosition / imgRes;
            var offset = coordinateRange * fraction;
            return coordinateStart + offset;
        }

        public static double ShortestDistance(Coordinate coordinate, IEnumerable<Coordinate> coordinates)
        {
            var distance = double.MaxValue;
            foreach (var point in coordinates)
            {
                double newDistance = point.Distance(coordinate);
                
                if (newDistance < distance)
                {
                    distance = newDistance;
                }
            }

            var distanceDegrees = Math.Sqrt(distance);

            var distanceKm = 88.905 * distanceDegrees;

            return distanceKm;
        }
    }
}