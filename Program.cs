﻿using System;
using System.IO;
using System.Linq;
using OsmSharp;
using OsmSharp.Streams;
using OsmSharp.Geo;
using OsmSharp.Geo.Streams;
using System.Collections.Generic;
using GeoAPI.Geometries;
using Newtonsoft.Json;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using System.Threading.Tasks;
using static DistanceMap.Geometry;

namespace DistanceMap
{
    class Program
    {
        static void Main(string[] args)
        {
            const int processes = 8;

            var coordinates = ComputeOrLoadCoordinates("./australia.osm.pbf", "./coordinates.json");

            // performance optimisation for dev. remove this.
            Random ran = new Random();
            //coordinates = coordinates.Where(c => ran.NextDouble() < 0.02).ToList();

            const int xRes = 1920*2;
            const int yRes = 1080*2;

            const double xCoordinateStart = 112.41;
            const double xCoordinateEnd = 154.69;

            const double yCoordinateStart = -44.65;
            const double yCoordinateEnd = -9.80;

            var xCoordinateRange = xCoordinateEnd - xCoordinateStart;
            var yCoordinateRange = yCoordinateEnd - yCoordinateStart;

            var tasks = new List<Task<IEnumerable<PixelResult>>>();
            for (int chunkNumber = 0; chunkNumber < processes; chunkNumber++)
            {
                var thisChunk = chunkNumber;
                var yStart = yRes * chunkNumber / processes;
                var yEnd = yStart + yRes / processes;

                double yChunkStart = yCoordinateStart + (yCoordinateRange * (double)chunkNumber / (double)processes);
                double yChunkEnd = yChunkStart + yCoordinateRange / processes;
                double yChunkRange = yChunkEnd - yChunkStart;

                var rowSize = yRes / processes;

                tasks.Add(ComputeBlock(
                    xRes,
                    yRes/processes,
                    xCoordinateStart, 
                    xCoordinateRange, 
                    yChunkStart, 
                    yChunkRange, 
                    coordinates).ContinueWith(result => 
                        result.Result.Select(q => new PixelResult(q.imgX, q.imgY + (thisChunk * rowSize), q.distance))
                    )
                );
            }

            var resultTask = Task.WhenAll(tasks);
            var finish = resultTask.ContinueWith(completedTasks =>
            {
                using (Image<Rgba32> image = new Image<Rgba32>(xRes, yRes))
                {
                    var results = completedTasks.Result;
                    foreach (var r in results)
                    {
                        var yMin = r.Min(q => q.imgY);
                        var yMax = r.Max(q => q.imgY);
                        foreach (var pix in r)
                        {
                            image[pix.imgX, (yRes - 1) - pix.imgY] = ColourFromDistance(pix.distance);
                        }
                    }
                    using (var fileStream = new FileStream("output.png", FileMode.Create))
                    {
                        image.SaveAsPng(fileStream);
                    }
                }
            });

            finish.Wait();

            Console.WriteLine("Done!");
        }

        private readonly static object syncLock = new object();

        private static Task<List<PixelResult>> ComputeBlock(
            int xRes,
            int yRes,
            double xCoordinateStart,
            double xCoordinateRange,
            double yCoordinateStart,
            double yCoordinateRange,
            IEnumerable<Coordinate> coordinates)
        {
            return Task.Run(() =>
            {
                DateTime start = DateTime.Now;        
                var result = new List<PixelResult>();
                int percent = 0;
                for (int imgX = 0; imgX < xRes; imgX++)
                {
                    for (int imgY = 0; imgY < yRes; imgY++)
                    {
                        var mapX = ImageCoordinateToMapCoordinate(imgX, xRes, xCoordinateStart, xCoordinateRange);
                        var mapY = ImageCoordinateToMapCoordinate(imgY, yRes, yCoordinateStart, yCoordinateRange);

                        var coord = new Coordinate(mapX, mapY);
                        var distance = ShortestDistance(coord, coordinates);

                        result.Add(new PixelResult(imgX, imgY, distance));
                    }
                    int newPercent = (int)(((double)imgX / (double)xRes) * 100);
                    if (newPercent > percent+4)
                    {
                        percent = newPercent;
                        Console.WriteLine($"Chunk {(int)percent}%");
                    }
                }

                Console.WriteLine($"Finished chunk in {(DateTime.Now - start).TotalSeconds} seconds");
                
                return result;
            });
        }

        const double MIN_DISTANCE = 20.0;
        const double MAX_DISTANCE = 80.0;
        private static Random ran = new Random();
        private static Rgba32 ColourFromDistance(double distance)
        {
            if (distance < MIN_DISTANCE) return Rgba32.Black;
            if (distance > MAX_DISTANCE) return new Rgba32(255, (byte)((double)255/2), (byte)((double)255/2));

            var normalised = (distance - MIN_DISTANCE) / (MAX_DISTANCE - MIN_DISTANCE);
            //var value = Math.Log(1 + normalised) / Math.Log(1 + MAX_DISTANCE);

            var b = (byte)(255.0D * normalised);

            var result =  new Rgba32(b, (byte)((double)b/2), (byte)((double)b/2));

            return result;
        }

        static IEnumerable<Coordinate> ComputeOrLoadCoordinates(string inputSource, string coordinateFile)
        {
            IEnumerable<Coordinate> result;
            JsonSerializer serializer = new JsonSerializer();
            serializer.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            if (new FileInfo(coordinateFile).Exists)
            {
                using (StreamReader file = File.OpenText(coordinateFile))
                {
                    result = (IEnumerable<Coordinate>)serializer.Deserialize(file, typeof(IEnumerable<Coordinate>));
                }
            }
            else
            {
                using (var fileStream = new FileInfo(@"./australia.osm.pbf").OpenRead())
                {
                    var source = new PBFOsmStreamSource(fileStream);
                    var roads = source.FilterNodes(node => node.Tags.ContainsKey("highway"));
                    var roadsComplete = roads.ToFeatureSource();

                    result = roadsComplete.Where(geo => geo.Geometry != null && geo.Geometry.Coordinate != null).Select(geo => geo.Geometry.Coordinate);

                    using (StreamWriter file = File.CreateText(coordinateFile))
                    {
                        serializer.Serialize(file, result);
                    }
                }
            }

            return result.ToList();
        }
    }

    struct PixelResult
    {
        public readonly int imgX;
        public readonly int imgY;
        public readonly double distance;

        public PixelResult(int imgX, int imgY, double distance)
        {
            this.imgX = imgX;
            this.imgY = imgY;
            this.distance = distance;
        }
    }
}
