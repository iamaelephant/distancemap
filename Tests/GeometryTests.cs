using System;
using Xunit;
using static DistanceMap.Geometry;

namespace DistanceMap.Tests
{
    public class GeometryTests
    {
        [Fact]
        public void ImageCoordinateToMapCoordinate_Position_0_returns_start()
        {
            var result = ImageCoordinateToMapCoordinate(0, 900, 23.4, 18.5);

            Assert.Equal(23.4, result);
        }

        [Fact]
        public void ImageCoordinateToMapCoordinate_Position_10_of_20_returns_halfway()
        {
            var result = ImageCoordinateToMapCoordinate(10, 20, 40.0, 40.0);

            Assert.Equal(60.0, result);
        }

        [Fact]
        public void ImageCoordinateToMapCoordinate_Position_50_of_50_returns_end()
        {
            var result = ImageCoordinateToMapCoordinate(50, 50, 86.5, 33.6);

            Assert.Equal(120.1, result);
        }
    }
}